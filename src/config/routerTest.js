import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import LoginCheck from '../screens/LoginCheck';
import LoginOrRegister from '../screens/login/LoginOrRegister';
import Main from '../screens/main/Main';
import Edit from '../screens/main/editing/Edit';

const AppStack = createStackNavigator({ Main, Edit });
const AuthStack = createStackNavigator({ LoginOrRegister });

export default createSwitchNavigator(
  {
    LoginCheck,
    Main: AppStack,
    LoginOrRegister: AuthStack,
  },
  {
    initialRouteName: 'LoginCheck',
  },
);
