import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import CalendarReducer from './CalendarReducer';
import NewDataReducer from './NewDataReducer';

export default combineReducers({
  authReducer: AuthReducer,
  calendarReducer: CalendarReducer,
  newDataReducer: NewDataReducer,
});
