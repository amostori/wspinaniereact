import { FETCH_SUCCESS, FETCH_DAY_SUCCESS } from '../actions/types';

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_SUCCESS:
      return action.payload;
    case FETCH_DAY_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};
