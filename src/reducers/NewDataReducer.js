import { DATA_CREATE } from '../actions/types';

const INITIAL_STATE = { text: 'Lambada3', descrip: 'Hejka3', date: '2019-03-17' };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case DATA_CREATE:
      return INITIAL_STATE;
    default:
      return state;
  }
};
