import React from 'react';
import PropTypes from 'prop-types';
import firebase from '@firebase/app';
import EStyleSheet from 'react-native-extended-stylesheet';
import '@firebase/auth';
import { View, Image } from 'react-native';
import { Spinner } from '../common';

const styles = EStyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '$myBackground',
  },
  second: {
    justifyContent: 'center',
  },
  image: {
    // flex: 1,
    // justifyContent: 'center',
    marginTop: 50,
    // alignItems: 'center',
    height: 280,
    width: 280,
  },
});

class LoginCheck extends React.Component {
  static propTypes = {
    navigation: PropTypes.any,
  };

  componentWillMount() {
    const { navigation } = this.props;
    firebase
      .auth()
      .onAuthStateChanged(user => navigation.navigate(user ? 'Main' : 'LoginOrRegister'));
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.second}>
          <Image
            source={require('./images/loginPicture.png')}
            style={styles.image}
            resizeMode="contain"
          />
          <Spinner size="large" />
        </View>
      </View>
    );
  }
}

export default LoginCheck;
