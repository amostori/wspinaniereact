import React from 'react';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';
import EStyleSheet from 'react-native-extended-stylesheet';
import _ from 'lodash';
import { connect } from 'react-redux';
import {
  View, Button, Text, YellowBox, TouchableOpacity,
} from 'react-native';
import firebase from '@firebase/app';
import '@firebase/auth';
import moment from 'moment';
// import momentLocal from 'moment/min/moment-with-locales';
import 'moment/min/locales';
import { LocaleConfig, Agenda } from 'react-native-calendars';
import { itemsFetch, itemsFetchInDay } from '../../actions';

LocaleConfig.locales.pl = {
  monthNames: [
    'Styczeń',
    'Luty',
    'Marzec',
    'Kwiecień',
    'Maj',
    'Czerwiec',
    'Lipiec',
    'Sierpień',
    'Wrzesień',
    'Październik',
    'Listopad',
    'Grudzień',
  ],
  monthNamesShort: [
    'Stcz.',
    'Lut',
    'Marz',
    'Kwi',
    'Maj',
    'Czer.',
    'Lip.',
    'Sier.',
    'Wrz.',
    'Paź.',
    'Lis.',
    'Gru.',
  ],
  dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
  dayNamesShort: ['Ndz.', 'Pon.', 'Wto.', 'Śro.', 'Czw.', 'Pią.', 'Sob.'],
};

LocaleConfig.defaultLocale = 'pl';

const styles = EStyleSheet.create({
  $myBlue: '#00adf5',
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
  },
});

class Main extends React.Component {
  static propTypes = {
    tempData: PropTypes.any,
    navigation: PropTypes.object,
    itemsFetch: PropTypes.func,
  };

  static navigationOptions = ({ navigation }) => ({
    headerRight: <Button onPress={() => firebase.auth().signOut()} title="Log out" color="red" />,
    title: navigation.getParam('date', 'dsf'),
  });

  constructor(props) {
    super(props);
    this.construct();
    moment.locale('pl');
    this.state = {
      currentDate: moment(new Date()).format('LL'),
      markedDate: moment(new Date()).format('YYYY-MM-DD'),
    };
  }

  componentWillMount() {
    // eslint-disable-next-line no-shadow
    const { itemsFetch, navigation } = this.props;
    const { currentDate } = this.state;
    navigation.setParams({ date: currentDate });
    itemsFetch();
  }

  componentDidMount() {
    const { navigation } = this.props;
    const { currentDate } = this.state;
    navigation.setParams({ date: currentDate });
  }

  onAddPress = (date) => {
    const { navigation } = this.props;
    console.log('Day sent: ', date);
    navigation.navigate('Edit', { day: date });
  };

  renderItem = item => (
    <View style={[styles.item]}>
      <TouchableOpacity onPress={this.onDayPress}>
        <View>
          <Text>{item.text}</Text>
          <Text>{item.descrip}</Text>
          <Text>{item.descrip}</Text>
          <Text>{item.descrip}</Text>
          <Text>{item.date}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );

  renderEmptyDate = () => {
    const { markedDate } = this.state;
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center', height: 100 }}>
        <TouchableOpacity onPress={() => this.onAddPress(markedDate)}>
          <Ionicons name="md-add" size={64} color={styles.$myBlue} />
        </TouchableOpacity>
      </View>
    );
  };

  rowHasChanged = (r1, r2) => r1.name !== r2.name;

  construct = () => {
    YellowBox.ignoreWarnings(['Setting a timer']);
  };

  returnStringDate = (day) => {
    let monthDay = '';
    let dayOfMonth = '';
    // const { month } = this.day;

    if (day.month < 10) {
      monthDay = `0${day.month}`;
    } else {
      monthDay = day.month;
    }

    if (day.day < 10) {
      dayOfMonth = `0${day.day}`;
    } else {
      dayOfMonth = day.day;
    }

    return `${day.year}-${monthDay}-${dayOfMonth}`;
  };

  onDayPress = (day) => {
    const { navigation } = this.props;
    const date = this.returnStringDate(day);
    navigation.setParams({ date });
    this.setState({
      markedDate: date,
    });
  };

  renderDay = (day) => {
    if (day) {
      let data = '';
      let dayOfMonth = '';

      if (day.month < 10) {
        data = `0${day.month}`;
      } else {
        data = day.month;
      }

      if (day.day < 10) {
        dayOfMonth = `0${day.day}`;
      } else {
        dayOfMonth = day.day;
      }

      const stringDate = this.returnStringDate(day);
      return (
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 16,
            alignItems: 'center',
          }}
        >
          <View style={{ flexDirection: 'column' }}>
            <TouchableOpacity
              onPress={() => this.onAddPress(stringDate)}
              style={{ alignItems: 'center' }}
            >
              <Text style={{ color: EStyleSheet.value('$myBlue'), fontSize: 24 }}>
                {dayOfMonth}
.
                {data}
              </Text>
              <Ionicons name="md-add" size={32} color={styles.$myBlue} />
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    return <View style={{ marginLeft: 16 }} />;
  };

  render() {
    // eslint-disable-next-line no-shadow
    const { tempData } = this.props;
    return (
      <Agenda
        items={tempData}
        loadItemsForMonth={this.returnItems}
        onDayPress={this.onDayPress}
        selected={new Date()}
        renderItem={this.renderItem}
        renderDay={this.renderDay}
        renderEmptyData={this.renderEmptyDate}
        rowHasChanged={this.rowHasChanged}
      />
    );
  }
}

const mapStateToProps = (state) => {
  const dane = _.map(state.calendarReducer, (val, uid) => ({ ...val, uid }));
  const tempData = {};
  /*

[
  {
    "date": "2019-03-06",
    "descrip": "Hejka",
    "text": "Lambada2",
    "uid": "-L_wZsNhNY72ZuXDuZr4",
  },
  {
    "date": "2019-03-06",
    "descrip": "Hejka",
    "text": "Lambada2",
    "uid": "-L_wZvfaA6HzE8H6Oo4h",
  }
]

*/
  let strTime = '';
  for (let i = 0; i < dane.length; i += 1) {
    strTime = dane[i].date;
    if (!tempData[strTime]) {
      // items[strTime] = undefined więc !items[strTime] oznacza true
      // jeśli items[strTime] jest undefined zrób:
      tempData[strTime] = [];
      // w obiekcie tempData utwórz obiekt o kluczu strTime i wartości []
      for (let j = 0; j < dane.length; j += 1) {
        if (dane[j].date === strTime) {
          // jeśli data jest taka jak strTime dodaj dane do tablicy
          tempData[strTime].push({
            date: dane[j].date,
            descrip: dane[j].descrip,
            text: dane.text,
          });
        }
      }
    }
  }

  return { tempData };
};

export default connect(
  mapStateToProps,
  { itemsFetch, itemsFetchInDay },
)(Main);
