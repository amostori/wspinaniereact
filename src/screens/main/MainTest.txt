import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import {
  View, Button, Text, StyleSheet, TouchableHighlight,
} from 'react-native';
import firebase from '@firebase/app';
import '@firebase/auth';
import { LocaleConfig, Agenda } from 'react-native-calendars';
import { itemsFetch } from '../../actions';
// import styles from './styles';

LocaleConfig.locales.pl = {
  monthNames: [
    'Styczeń',
    'Luty',
    'Marzec',
    'Kwiecień',
    'Maj',
    'Czerwiec',
    'Lipiec',
    'Sierpień',
    'Wrzesień',
    'Październik',
    'Listopad',
    'Grudzień',
  ],
  monthNamesShort: [
    'Stcz.',
    'Lut',
    'Marz',
    'Kwi',
    'Maj',
    'Czer.',
    'Lip.',
    'Sier.',
    'Wrz.',
    'Paź.',
    'Lis.',
    'Gru.',
  ],
  dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
  dayNamesShort: ['Ndz.', 'Pon.', 'Wto.', 'Śro.', 'Czw.', 'Pią.', 'Sob.'],
};

LocaleConfig.defaultLocale = 'pl';

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
  },
});

class Main extends React.Component {
  static propTypes = {
    dane: PropTypes.any,
    navigation: PropTypes.object,
    itemsFetch: PropTypes.func,
  };

  static navigationOptions = {
    headerRight: <Button onPress={() => firebase.auth().signOut()} title="Log out" color="red" />,
    headerTitle: 'Main',
  };

  state = {
    items: {},
  };

  componentWillMount() {
    const { itemsFetch } = this.props;
    itemsFetch();
  }

  onDayPress = () => {
    const { navigation } = this.props;
    navigation.navigate('Edit');
  };

  onItemPressed = () => this.onDayPress();

  renderItem = item => (
    <View style={[styles.item]}>
      <TouchableHighlight onPress={this.onItemPressed}>
        <View>
          <Text>{item.text}</Text>
          <Text>{item.descrip}</Text>
          <Text>{item.date}</Text>
        </View>
      </TouchableHighlight>
    </View>
  );

  renderEmptyDate = () => (
    <View style={styles.emptyDate}>
      <Text>This is empty date!</Text>
    </View>
  );

  rowHasChanged = (r1, r2) => r1.name !== r2.name;

  timeToString = (time) => {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  };

  render() {
    const { dane } = this.props;
    return (
      <Agenda
        items={{
          '2019-03-04': dane,
        }}
        selected="2019-03-04"
        renderItem={this.renderItem}
        renderEmptyDate={this.renderEmptyDate}
        rowHasChanged={this.rowHasChanged}
      />
    );
  }
}

const mapStateToProps = (state) => {
  const items = {};

  const dane = _.map(state.calendarReducer, (val, uid) => ({ ...val, uid }));
  const dane2 = _.map(dane, (val, uid) => ({ ...val, uid }));

  // console.log('Dane z Main: ', dane);
  if (dane[1]) {
   // console.log('Dane.date z Main: ', dane[1].date);
    // console.log('Dane2.uid z Main: ', dane2);
    // console.log('Dane2.uid z Main: ', dane[1].uid);
  }
  /*
   const newItems = {};
      Object.keys(this.state.items).forEach(key => {newItems[key] = this.state.items[key];});
      this.setState({
        items: newItems
      });

    metoda ta tworzy array obiektów
  */
  const obiekt = {};
  //  Object.keys(dane)
  return { dane };
};

export default connect(
  mapStateToProps,
  { itemsFetch },
)(Main);


 // ///////////////////////////////////

  loadItems = (day) => {
    const { items } = this.state;
    setTimeout(() => {
      for (let i = 0; i < 2; i += 1) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = this.timeToString(time);
        // items[strTime] = undefined
        if (!items[strTime]) {
          items[strTime] = [];
          // pierwsze przejście iteracji i (dodawanie obiektu, którego kluczem jest data a wartością array)
          // items = {'2019-03-30': []}
          // drugie przejście
          /* items = {
                        '2019-03-30': [{name: 'Item for 2019-03-30', height: 30}],
                        '2019-03-31': [],
                      }

          */
          const numItems = Math.floor(Math.random() * 2);
          // numItems losowa liczba od 0 do 2
          for (let j = 0; j < numItems; j += 1) {
            items[strTime].push({
              name: `Item for ${strTime}`,
              height: Math.max(50, Math.floor(Math.random() * 150)),
            });
            // po pierwszym przejściu j
            // items = {'2019-03-30': [{name: 'Item for 2019-03-30', height: 30}]}
            // po drugim przejściu j
            // items = {'2019-03-30': [{name: 'Item for 2019-03-30', height: 30},{name: 'Item for 2019-03-30', height: 30}]}
          } // koniec iteracji j czyli pushowania do array items.strTime
        }
      } // koniec iteracji i

      const newItems = {};
      Object.keys(this.state.items).forEach((key) => {
        // key = kluczem jest data
        newItems[key] = this.state.items[key];
        // newItems = { 'data': [{obiekty przepisane z state.items}]}
      });
      this.setState({
        items: newItems,
      });
      // console.log('newItems: ', newItems);
    }, 1000);
    // console.log(`Load Items for ${day.year}-${day.month}`);
  };

  returnItems = () => {
    const { tempData } = this.props;
    return { tempData };
  };
  // ////////////////////////////////////