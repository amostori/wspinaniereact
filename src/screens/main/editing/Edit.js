import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { Input, Button } from '../../../common';

import { exerciesCreate, itemsFetch } from '../../../actions';

class Edit extends React.Component {
  static propTypes = {
    text: PropTypes.string,
    descrip: PropTypes.string,
    date: PropTypes.string,
    exerciesCreate: PropTypes.func,
    itemsFetch: PropTypes.func,
  };

  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('day', 'dsf'),
  });

  onPress = () => {
    const {
      exerciesCreate, text, descrip, date,
    } = this.props;
    exerciesCreate({ text, descrip, date });
  };

  onPressFetch = () => {
    const { itemsFetch } = this.props;
    itemsFetch();
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'space-evenly' }}>
        <Text>Editing</Text>
        <Input />
        <Button style={{ height: 20, marginBottom: 20 }} onPress={this.onPress}>
          Button
        </Button>
        <Button style={{ height: 20, marginBottom: 20 }} onPress={this.onPressFetch}>
          Fetch
        </Button>
      </View>
    );
  }
}

const mapStateToProps = ({ newDataReducer }) => {
  const { text, descrip, date } = newDataReducer;

  return { text, descrip, date };
};

export default connect(
  mapStateToProps,
  { exerciesCreate, itemsFetch },
)(Edit);
