import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import {
  View, TouchableOpacity, Image, KeyboardAvoidingView,
} from 'react-native';
import {
  onEmailEnter,
  onPasswordEnter,
  onLoginAction,
  onRegisterAction,
  onRegister,
} from '../../actions';
import {
  CardSection, Input, Button, Spinner,
} from '../../common';
import Tekst from '../../common/Tekst';

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f7fc',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  image: {
    height: 100,
    width: 100,
  },
  firstPart: {
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  secondPart: {
    marginBottom: 20,
    alignItems: 'center',
  },
  textContainer: {
    justifyContent: 'flex-start',
  },
  title: {
    fontSize: 20,
    marginBottom: 20,
    marginTop: 40,
  },
  introduction: {
    color: '#afafb0',
    marginBottom: 40,
  },
  smallText: {},
  register: {
    color: '$myBlue',
  },
  textError: {
    color: 'red',
  },
});

class LoginOrRegister extends Component {
  static navigationOptions = {
    header: null,
  };

  static propTypes = {
    email: PropTypes.string,
    password: PropTypes.string,
    onEmailEnter: PropTypes.func,
    onPasswordEnter: PropTypes.func,
    onRegisterAction: PropTypes.func,
    onRegister: PropTypes.func,
    error: PropTypes.any,
    loading: PropTypes.bool,
    registering: PropTypes.bool,
  };

  onEmailChange = (text) => {
    const { onEmailEnter } = this.props;
    onEmailEnter(text);
  };

  onChangedPassword = (text) => {
    const { onPasswordEnter } = this.props;
    onPasswordEnter(text);
  };

  onLoginPressed = () => {
    const { onLoginAction, email, password } = this.props;
    onLoginAction({ email, password });
  };

  onRegisterPressed = () => {
    const { onRegister, email, password } = this.props;
    onRegister({ email, password });
  };

  handleOnRegister = () => {
    const { onRegisterAction } = this.props;
    onRegisterAction();
  };

  renderButton = () => {
    const { registering, loading } = this.props;
    if (loading) {
      return <Spinner size="small" />;
    }
    if (registering) {
      return <Button onPress={this.onRegisterPressed}>Zarejestruj</Button>;
    }
    return <Button onPress={this.onLoginPressed}>Zaloguj</Button>;
  };

  renderError = () => {
    const { error } = this.props;
    if (error) {
      return (
        <Tekst style={styles.textError}>
          Nieprawidłowe hasło lub email. Sprawdź połączenie internetowe.
        </Tekst>
      );
    }
    return null;
  };

  render() {
    const { email, password } = this.props;
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <View style={styles.firstPart}>
          <View style={styles.textContainer}>
            <Tekst style={styles.title}>Trening wspinaczkowy</Tekst>
            <Tekst style={styles.introduction}>
              Witaj w aplikacji. Zaloguj się lub stwórz nowe konto.
            </Tekst>
          </View>
          <CardSection>
            <Input
              label="Email"
              value={email}
              placeholder="email@gmail.com"
              onChangeText={this.onEmailChange}
            />
          </CardSection>
          <CardSection>
            <Input
              label="Hasło"
              value={password}
              secureTextEntry
              placeholder="hasło"
              onChangeText={this.onChangedPassword}
            />
          </CardSection>
          {this.renderError()}
          <CardSection style={{ marginTop: 40 }}>{this.renderButton()}</CardSection>
        </View>
        <Image
          source={require('../images/loginPicture.png')}
          style={styles.image}
          resizeMode="contain"
        />
        <View style={styles.secondPart}>
          <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.handleOnRegister}>
            <CardSection style={{ backgroundColor: '#f5f7fc' }}>
              <Tekst style={styles.smallText}>Nie masz konta? </Tekst>
              <Tekst style={styles.register}>Zarejestruj się!</Tekst>
            </CardSection>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = ({ authReducer }) => {
  const {
    email, password, user, error, loading, registering,
  } = authReducer;
  return {
    email,
    password,
    user,
    error,
    loading,
    registering,
  };
};

export default connect(
  mapStateToProps,
  {
    onEmailEnter,
    onPasswordEnter,
    onLoginAction,
    onRegisterAction,
    onRegister,
  },
)(LoginOrRegister);
