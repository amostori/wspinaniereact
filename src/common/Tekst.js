import React from 'react';

import PropTypes from 'prop-types';
import { Text } from 'react-native';

const styles = {
  textStyle: {
    color: '#343434',
    // color: '#afafb0',
    position: 'relative',
  },
};

const Tekst = ({ style, children }) => <Text style={[styles.textStyle, style]}>{children}</Text>;

export default Tekst;

Tekst.propTypes = {
  style: PropTypes.any,
  children: PropTypes.any,
};
