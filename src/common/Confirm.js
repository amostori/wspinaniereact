import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, Modal } from 'react-native';
import { CardSection } from './CardSection';
import { Button } from './Button';

const styles = {
  cardSectionStyle: {
    justifyContent: 'center',
  },
  textStyle: {
    flex: 1,
    fontSize: 18,
    textAlign: 'center',
    lineHeight: 40,
  },
  containerStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    position: 'relative',
    flex: 1,
    justifyContent: 'center',
  },
};

const Confirm = ({
  children, visible, onAccept, onDecline,
}) => {
  const { containerStyle, textStyle, cardSectionStyle } = styles;
  // onRequesrClose jest wymagane przez Androida
  return (
    <Modal visible={visible} transparent animationType="slide" onRequestClose={() => {}}>
      <View style={containerStyle}>
        <CardSection style={cardSectionStyle}>
          <Text style={textStyle}>{children}</Text>
        </CardSection>

        <CardSection>
          <Button onPress={onAccept}>Yes</Button>
          <Button onPress={onDecline}>No</Button>
        </CardSection>
      </View>
    </Modal>
  );
};

Confirm.propTypes = {
  children: PropTypes.any,
  visible: PropTypes.bool,
  onAccept: PropTypes.func,
  onDecline: PropTypes.func,
};

export { Confirm };
export const dupa = null;
