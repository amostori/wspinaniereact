import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import Tekst from './Tekst';

const Button = ({ onPress, children }) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Tekst style={textStyle}>{children}</Tekst>
    </TouchableOpacity>
  );
};

const styles = EStyleSheet.create({
  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
  },
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '$myBlue',
    borderRadius: 2,
    borderWidth: 1,
    borderColor: '$myBlue',
    marginLeft: 5,
    marginRight: 5,
  },
});

export { Button };
