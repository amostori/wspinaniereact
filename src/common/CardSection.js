import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

const styles = {
  containerStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: '#f5f7fc',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative',
  },
};

const CardSection = (props) => {
  const { style, children } = props;
  return <View style={[styles.containerStyle, style]}>{children}</View>;
};

CardSection.propTypes = {
  style: PropTypes.any,
  children: PropTypes.any,
};

export { CardSection };
