import firebase from '@firebase/app';
import '@firebase/database';
import moment from 'moment';
import 'moment/min/moment-with-locales';
import NavigationService from '../../NavigationService';
import { FETCH_DAY_SUCCESS, FETCH_SUCCESS } from './types';

export const refreshItems = () => null;

export const exerciesCreate = ({ text, descrip, date }) => {
  const { currentUser } = firebase.auth();
  // const date = '2019-03-10';
  return () => {
    firebase
      .database()
      .ref(`users/${currentUser.uid}`)
      .push({ text, descrip, date })
      .then(() => {
        NavigationService.navigate('Main');
      });
  };
};

export const itemsFetch = () => (dispatch) => {
  const { currentUser } = firebase.auth();
  firebase
    .database()
    .ref(`users/${currentUser.uid}`)
    .on('value', (snapshot) => {
      // console.log('CalendarAction, itemsFetch snapshot.val: ', snapshot.val());
      dispatch({
        type: FETCH_SUCCESS,
        payload: snapshot.val(),
      });
      moment.locale('pl');
      const myDate = moment(new Date()).format('LL');
      NavigationService.navigate('Main', { date: myDate });
    });
};

export const itemsFetchInDay = day => (dispatch) => {
  const { currentUser } = firebase.auth();
  firebase
    .database()
    .ref(`users/${currentUser.uid}/${day}`)
    .on('value', (snapshot) => {
      // console.log('CalendarAction, itemsFetch snapshot.val: ', snapshot.val());
      dispatch({
        type: FETCH_DAY_SUCCESS,
        payload: snapshot.val(),
      });
      NavigationService.navigate('Main');
    });
};
