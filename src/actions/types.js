export const EMAIL_ENTER = 'EMAIL_ENTER';
export const PASS_ENTER = 'PASS_ENTER';
export const LOGIN_ACTION = 'LOGIN_ACTION';
export const FONT_CHANGE = 'FONT_CHANGE';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOGIN_STARTED = 'LOGIN_STARTED';
export const REGISTERING = 'REGISTERING';
export const ITEM_CREATED = 'ITEM_CREATED';
export const FETCH_SUCCESS = 'FETCH_SUCCESS';
export const DATA_CREATE = 'DATA_CREATE';
export const FETCH_DAY_SUCCESS = 'FETCH_DAY_SUCCESS';
