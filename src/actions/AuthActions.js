import firebase from '@firebase/app';
import '@firebase/auth';
// import { Actions } from 'react-native-router-flux';
import NavigationService from '../../NavigationService';
import {
  EMAIL_ENTER,
  PASS_ENTER,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGIN_STARTED,
  REGISTERING,
} from './types';

export const onEmailEnter = text => ({
  type: EMAIL_ENTER,
  payload: text,
});

export const onRegisterAction = () => ({
  type: REGISTERING,
});

export const onPasswordEnter = password => ({
  type: PASS_ENTER,
  payload: password,
});

export const onLoginSucces = (dispatch, user) => {
  dispatch({
    type: LOGIN_SUCCESS,
    payload: user,
  });
  NavigationService.navigate('Main', {
    userName: 'Lucy',
  });
};

export const onLoginFailer = (dispatch) => {
  dispatch({
    type: LOGIN_FAILED,
  });
};

export const onLoginAction = ({ email, password }) => (dispatch) => {
  dispatch({
    type: LOGIN_STARTED,
  });
  // firebase
  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(user => onLoginSucces(dispatch, user))
    .catch((error) => {
      onLoginFailer(dispatch);
    });
};

export const onRegister = ({ email, password }) => (dispatch) => {
  dispatch({
    type: LOGIN_STARTED,
  });
  // firebase
  firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then(user => onLoginSucces(dispatch, user))
    .catch((error) => {
      onLoginFailer(dispatch);
    });
};
