import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import firebase from '@firebase/app';
import '@firebase/auth';
import { createAppContainer } from 'react-navigation';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationService from './NavigationService';
import reducers from './src/reducers';
import Navigator from './src/config/routerTest';

const AppContainer = createAppContainer(Navigator);
EStyleSheet.build({
  $border: '#e2e2e2',
  $myBackground: '#f5f7fc',
  $myBlue: '#00adf5',
  // $outline: 1,
});
class App extends Component {
  componentWillMount() {
    const config = {
      apiKey: 'AIzaSyCa534aYU4h6w08xXejua8RHfTesttm9To',
      authDomain: 'wspinanie-47eb5.firebaseapp.com',
      databaseURL: 'https://wspinanie-47eb5.firebaseio.com',
      projectId: 'wspinanie-47eb5',
      storageBucket: 'wspinanie-47eb5.appspot.com',
      messagingSenderId: '1015744092058',
    };
    firebase.initializeApp(config);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <AppContainer
          ref={(navigatorRef) => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}
// NavigationService.setTopLevelNavigator pozwala na uruchamianie metody navigate w plikach, które
// nie są klasą. Głównie potrzebujemy tego w AuthAction do uruchomienia Main w LOGIN_SUCCESS.
export default App;
